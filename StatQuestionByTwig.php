<?php
/**
 * Allow to use a twig file to render question text.
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.1.6
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class StatQuestionByTwig extends PluginBase
{

    static protected $name = 'StatQuestionByTwig';
    static protected $description = 'Allow to use a twig file to render question text.';

    protected $storage = 'DbStorage';

    protected $settings = array(
        'warningIssue' => array(
            'type' => 'info',
            'content' => "<div class='alert alert-warning'>" ."If you activate this plugin, user with right on survey content, but without rights on survey response can read responses using this plugin." . "</div>",
        ),
    );
    /* @var int $sid */
    private $sid;

    /**
    * Add function to be used in beforeQuestionRender event and to attriubute
    */
    public function init()
    {
        $this->subscribe('beforeQuestionRender','renderByTwig');
        $this->subscribe('newQuestionAttributes');
    }

    /**
    * Replace 
    */
    public function renderByTwig()
    {
        if ($this->getEvent()->get('type') != 'X') {
            return;
        }
        $oEvent = $this->getEvent();
        $qid = $oEvent->get('qid');
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        if (empty($aAttributes['twigFile'])) {
            return;
        }
        $sid = $oEvent->get('surveyId');
        $twigFile = $aAttributes['twigFile'];
        $ext = pathinfo($twigFile, PATHINFO_EXTENSION);
        if (empty($ext)) {
            $twigFile .= '.twig';
        }
        $surveyDir = App()->getConfig('uploaddir') . DIRECTORY_SEPARATOR . '/surveys' . DIRECTORY_SEPARATOR . $sid . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR;
        /** Check validity of file name **/
        $completeTwigFile = $surveyDir . $twigFile;
        if(substr($completeTwigFile, 0, strlen($surveyDir)) !== $surveyDir) {
            $this->log(sprintf("[Security] Invalid filename %s in renderByTwig plugin", CHtml::encode($twigFile)), \CLogger::LEVEL_WARNING);
            return;
        }
        if (is_file($completeTwigFile)) {
            $this->sid = $sid;
            $this->subscribe('getPluginTwigPath', 'addSurveyFilesInTwigPath');
        } else {
            if (Permission::model()->hasSurveyPermission($sid, 'surveycontent', 'update')) {
                $message = "<div class='alert alert-warning'>" . sprintf($this->translate("Unable to find %s twig file in surveys file."), CHtml::encode($twigFile)) . "</div>";
                $oEvent->set('text', $oEvent->get('text') . $message);
            }
            return;
        }
        $oSurvey = Survey::model()->findByPk($sid);
        $useToken = !$oSurvey->getIsAnonymized() && $oSurvey->getHasTokensTable();
        $token = null;
        if ($useToken && $aAttributes['twigFileTokenUsage'] != 'no') {
            if (empty(App()->getRequest()->getParam('token')) && empty($_SESSION['survey_'.$sid]['token'])) {
                $message = "<div class='alert alert-warning'>" . $this->translate("Unable to use twig file without token.") . "</div>";
                $oEvent->set('text', $oEvent->get('text') . $message);
            }
            $token = App()->getRequest()->getParam('token');
            if(empty($token)) {
                $token = $_SESSION['survey_'.$sid]['token'];
            }
        }
        /* Some specific data */
        $srid = null;
        if (!empty($_SESSION['survey_'.$sid]['srid'])) {
            $srid = $_SESSION['survey_'.$sid]['srid'];
        }
        /* Return the data */
        $aReponses = $this->getData($sid, $qid, $token, $srid);
        /* try to find twig file */
        $renderData = array(
            'aSurveyInfo' => getSurveyInfo($oEvent->get('surveyId'), App()->getLanguage()),
            'aReponses' => $aReponses,
        );
        $oEvent->set('text',$oEvent->get('text') . App()->twigRenderer->renderPartial($twigFile,$renderData));
        $this->unsubscribe('getPluginTwigPath');
    }

    /**
     * Add current sid directory in twig
     */
    public function addSurveyFilesInTwigPath()
    {
        $sid = $this->sid;
        $viewPath = App()->getConfig('uploaddir') . "/surveys/{$sid}/files/";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
    * The attribute
    */
    public function newQuestionAttributes()
    {
        $twigAttributes = array(
            'twigFile' => array(
                'name' => 'twigFile',
                'types' => 'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 100,
                'inputtype' => 'text',
                'default' => '',
                'help' => $this->translate("Twig file uploaded in survey, must be a txt file with any extension, default extension is twig. In this twig file : you get array of response as array with code as key and answer as value."),
                'caption' => $this->translate('Twig file to use'),
            ),
            'twigFileFixedAnswers' => array(
                'name' => 'twigFileFixedAnswers',
                'types' => 'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 150,
                'inputtype' => 'singleselect',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'default' => 1,
                'help' => $this->translate("You can coose to export code, similar with export system. Answer in array can be code for single choice, get the shown answer and fix numeric value in the array."),
                'caption' => $this->translate('Return full answers'),
            ),
            'twigFileTokenUsage'=>array(
                'types'=>'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 200, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    'no' => gT('No'),
                    'token' => gT('Yes'),
                    'group' => $this->translate('Token Group (with responseListAndManage plugin)')
                ),
                'default' => 'token',
                'caption' => $this->translate('Usage of token.'),
            ),
            'twigFileSubmitted'=>array(
                'types'=>'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 250, /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'default' => 1,
                'caption' => $this->translate('Only submitted reponse.'),
                'help' => $this->translate('You can always check if submitted with <code>submitted</code> key.'),
            ),
            'twigFileFiltersField'=>array(
                'types'=>'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 300,
                'inputtype' => 'textarea',
                'default' => "",
                'expression' => 1,
                'help' => $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>), you can use Expressiona Manager in value, for filter, you can use <code>&gt;</code>, <code>&lt;</code> and other comparators.'),
                'caption' => $this->translate('Filter by.'),
            ),
            'twigFileOrderBy'=>array(
                'types' => 'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 400 , /* Own category */
                'inputtype' => 'text',
                'default' => "",
                'help' => $this->translate('Use question code for the columns to be ordered. Default “id DESC”, “datestamp ASC” for datestamped surveys.'),
                'caption' => $this->translate('Default order by'),
            ),
            'twigFileExcludeSrid'=>array(
                'types' => 'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 450 , /* Own category */
                'inputtype' => 'singleselect',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'default' => 1,
                'caption' => $this->translate('Exclude current response'),
            ),
            'twigFileLimit'=>array(
                'types' => 'X',
                'category' => $this->translate("Statistic by Twig"),
                'sortorder' => 500 , /* Own category */
                'inputtype' => 'integer',
                'min'=>'0',
                'default' => 100,
                'help' => $this->translate('Use 0 to get all avaiable respoinse according to filter.'),
                'caption' => $this->translate('Maximum number of responses to return'),
            ),
        );
        $this->getEvent()->append('questionAttributes', $twigAttributes);
    }

    /**
    * get response data for twig generation
    * @param integre $sid
    * @param integer $qid
    * @param string|null $token
    * @param integer|null $srid
    * @return array[]
    */
    private function getData($surveyid, $qid, $token, $srid)
    {
        $oSurvey = Survey::model()->findByPk($surveyid);
        if(!$oSurvey->getIsActive()) {
            return array();
        }
        $aAttributes =  $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        /* Go go */
        $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyid);
        $aCodeToColumn = array_flip($aColumnToCode);
        $availableColumns = SurveyDynamic::model($surveyid)->getAttributes();
        $fixAnswers = $aAttributes['twigFileFixedAnswers'];
        if ($fixAnswers) {
            $aQuestionAnswers = \getQuestionInformation\helpers\surveyAnswers::getAllQuestionsAnswers($surveyid, App()->getLanguage());
        }
        //tracevar($aQuestionAnswers);
        $criteria = new CDBCriteria;
        if ($aAttributes['twigFileSubmitted']) {
            $criteria->addCondition("submitdate IS NOT NULL");
        }
        if ($token) {
            switch($aAttributes['twigFileTokenUsage']) {
                case 'no':
                    /* must NOT happen */
                    break;
                case 'token':
                default:
                    $criteria->compare('token', $token);
                    break;
                case 'group':
                    $tokenList = \responseListAndManage\Utilities::getTokensList($surveyid, $token, false);
                    $criteria->addInCondition('token', $tokenList);
                    break;
            }
        }
        if($srid) {
            if ($aAttributes['twigFileExcludeSrid']) {
                $criteria->compare('id', "<>" . $srid);
            }
        }
        if ($aAttributes['twigFileFiltersField']) {
            $filtersField = trim($aAttributes['twigFileFiltersField']);
            $aFieldsLines = preg_split('/\r\n|\r|\n/', $filtersField, -1, PREG_SPLIT_NO_EMPTY);
            $aFiltersFields = array();
            foreach ($aFieldsLines as $aFieldLine) {
                if (!strpos($aFieldLine, ":")) {
                    continue; // Invalid line
                }
                $key = substr($aFieldLine, 0, strpos($aFieldLine, ":"));
                $value = substr($aFieldLine, strpos($aFieldLine, ":")+1);
                $value = \LimeExpressionManager::ProcessStepString($value, array(), 3, 1);
                if (array_key_exists($key, $availableColumns)) {
                    $aFiltersFields[$key] = $value;
                } elseif(isset($aCodeToColumn[$key])) {
                    $aFiltersFields[$aCodeToColumn[$key]] = $value;
                }
            }
            foreach ($aFiltersFields as $column => $value) {
                $criteria->compare(App()->getDb()->quoteColumnName($column), $value);
            }
        }

        $orderBy = trim($aAttributes['twigFileOrderBy']);
        $sFinalOrderBy = "";
        if (!empty($orderBy)) {
            $aOrdersBy = explode(",", $orderBy);
            $aOrderByFinal = array();
            foreach ($aOrdersBy as $sOrderBy) {
                $aOrderBy = explode(" ", trim($sOrderBy));
                $arrangement = "ASC";
                if (!empty($aOrderBy[1]) and strtoupper($aOrderBy[1]) == 'DESC') {
                    $arrangement = "DESC";
                }
                if (!empty($aOrderBy[0])) {
                    $orderColumn = null;
                    if (array_key_exists($aOrderBy[0], $availableColumns)) {
                        $aOrderByFinal[] = App()->db->quoteColumnName($aOrderBy[0]) . " " . $arrangement;
                    } elseif(isset($aCodeToColumn[$aOrderBy[0]])) {
                        $aOrderByFinal[] = App()->db->quoteColumnName($aCodeToColumn[$aOrderBy[0]]) . " " . $arrangement;
                    }
                }
            }
            $sFinalOrderBy = implode(",", $aOrderByFinal);
        }
        if (empty($sFinalOrderBy)) {
            $sFinalOrderBy = Yii::app()->db->quoteColumnName('id')." DESC";
            if (Survey::model()->findByPk($surveyid)->datestamp == "Y") {
                $sFinalOrderBy = Yii::app()->db->quoteColumnName('datestamp')." ASC";
            }
        }
        $criteria->order = $sFinalOrderBy;
        $twigFileLimit = intval($aAttributes['twigFileLimit']);
        if($twigFileLimit) {
            $criteria->limit = $twigFileLimit;
        }
        $oResponses = \Response::model($surveyid)->findAll($criteria);
        if(empty($oResponses)) {
            return array();
        }
        $aResponses = array();
        
        foreach($oResponses as $oResponse) {
            $aCoreResponse = $oResponse->getAttributes();
            $aResponse = array();
            foreach($aCoreResponse as $column => $value) {
                /* fix value */
                if ($fixAnswers && isset($aQuestionAnswers[$column])) {
                    $questionsAnswers = $aQuestionAnswers[$column];
                    switch($questionsAnswers['type']) {
                        case 'decimal':
                            if($value && $value[0] == ".") {
                                $value = "0" . $value;
                            }
                            if (strpos($value, ".")) {
                                $value = rtrim(rtrim($value, "0"), ".");
                            }
                            break;
                        case 'answer':
                            if(isset($questionsAnswers['answers'][$value])) {
                                $value = $questionsAnswers['answers'][$value];
                            }
                            break;
                        case 'freeanswer':
                            // No change
                            break;
                        default:
                            // No change
                    }
                }
                if (isset($aColumnToCode[$column])) {
                    $column = $aColumnToCode[$column];
                }

                $aResponse[$column] = $value;
                $aResponse['completed'] = null;
                if(!empty($aResponse['submitdate'])) {
                    $aResponse['completed'] = gT("Yes");
                }
            }
            $aResponses[$aCoreResponse['id']] = $aResponse;
        }
        return $aResponses;
    }
    /**
    * @see parent::gT
    */
    private function translate($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return $this->gT($sToTranslate,$sEscapeMode,$sLanguage);
    }

}
