# StatQuestionByTwig

Get list of response and show it with a twig file uploaded in surveys files.

**Warning** : If you activate this plugin, user with right on survey content, but without rights on survey response can read responses using this plugin.

Sample and demo : [Stat question by twig demo survey](https://demo.sondages.pro/966743)

## Usage

- On Display text question : you can use twig to produce HTML shown in quetsion text
- The file must be uploaded in survey ressources, no need to end with .twig
- Question settings :
    - _Twig file to use_ The name of the file, with extension if it's not twig.
    - _Return full answers_ : Similar to the export system of LimeSurvey
    - _Usage of token_ : Filter by current token if exist (and survey is not anonymous).
    - _Only submitted reponse_ : Return only submitted responses.
    - _Filter by_ : Extra filter to be used, you can use expression manager code. Each line can be a filter, column and value are separated by <code>:</code>. Value can use expression manager, and can use operator like [Yii compare](https://www.yiiframework.com/doc/api/1.1/CDbCriteria#compare-detail).
    - _'Default order by_ : Use question code or column name for the columns to be ordered. Default “id DESC”, “datestamp ASC” for datestamped surveys.
    - _Exclude current response_ Exclude or include current response
    - _Maximum number of responses to return_ The macimum number of mlines to return, to return all lines : set to 0.
- In twig you get the array of response in `aReponses` array
    - The key are the id of the `reponse`
    - Each response have whole answer with key from expression manager code (like VV export of LimeSurvey).

## Contribute, issue and support

- [Contribution are welcome](https://gitlab.com/SondagesPro/ExportAndStats/StatQuestionByTwig/-/merge_requests).
- Issue can be reported on [gitlab](https://gitlab.com/SondagesPro/ExportAndStats/StatQuestionByTwig/-/issues). **No support is done via issue on gitlab.** To report an issue : it's mandatory to send
    - A working lss and twig file
    - A clean detail of the issue and the way to reproduce on any server.
    - The exact limesurvey version
    - The exact plugin version
- If you need help to use this plugin and construct the witg file : you can [open a support ticket](https://support.sondages.pro/).

## Home page & Copyright
- HomePage <http://sondages.pro/>
- Copyright © 2021 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/) 
